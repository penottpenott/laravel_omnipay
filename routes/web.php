<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/pagar', [                    
	'uses' => 'OmnipayController@pagar_stripe',  
	'as'   => 'pagar_stripe'        
]);

Route::get('/pagar_paypal', [                    
	'uses' => 'OmnipayController@pagar_paypal',  
	'as'   => 'pagar_paypal'        
]);

Route::get('/pagar_payment_express', [                    
	'uses' => 'OmnipayController@pagar_payment_express',  
	'as'   => 'pagar_payment_express'        
]);
