<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Omnipay\Omnipay;

class OmnipayController extends Controller
{
	public function pagar_stripe()
    {  

	    $cardInput = [
			'number'      => '4242424242424242',
			'firstName'   => 'Aquiles Penott',
			'expiryMonth' => '03',
			'expiryYear'  => '16',
			'cvv'         => '333',
		];

		$gateway = Omnipay::create('Stripe');

		$gateway->setApiKey('sk_test_0Vw2anA5AN0Ha62pwOSj46SL00ksIDRmOi');

		//$formData = array('number' => '4242424242424242', 'expiryMonth' => '6', 'expiryYear' => '2030', 'cvv' => '123');
		$response = $gateway->purchase(array('amount' => '15.00', 'currency' => 'USD','token' => 'tok_visa', 'card' => $cardInput))->send();

		dd($response);
	}

	public function pagar_paypal()
    {  

	    $cardInput = [
			'number'      => '4032030956047162',
			'expiryMonth' => '01',
			'expiryYear'  => '2025',
			'cvv'         => '458',
		];

		$gateway = Omnipay::create('PayPal_Express');
		$gateway->setUsername('sb-ftt5d3777093_api1.business.example.com');
		$gateway->setPassword('K3VLXKZHW8YY7P25');
		$gateway->setSignature('A-2571xtKc7zfa3gSrNtvhvIgpLZAl-ZDvNr0ZSqpXV1wWHlqZ3PJyBs');
		$gateway->setTestMode(true);

		$response = $gateway->purchase(array(
			'amount' => '15.00', 
			'currency' => 'USD', 
			'returnUrl' => 'http://localhost/laravel/public/pagar_paypal',
            'cancelUrl' => 'http://localhost/laravel/public/pagar_paypal',
		))->send();

		dd($response);
	}

	public function pagar_payment_express()
    {  

	    $cardInput = [
			'number'      => '4032030956047162',
			'expiryMonth' => '01',
			'expiryYear'  => '2025',
			'cvv'         => '458',
		];

		$gateway = Omnipay::create('PaymentExpress_PxPay');
		/*$gateway->setUsername('sb-ftt5d3777093_api1.business.example.com');
		$gateway->setPassword('K3VLXKZHW8YY7P25');
		$gateway->setSignature('A-2571xtKc7zfa3gSrNtvhvIgpLZAl-ZDvNr0ZSqpXV1wWHlqZ3PJyBs');
		$gateway->setTestMode(true);*/

		dd($gateway);

		$response = $gateway->purchase(array(
			'amount' => '15.00', 
			'currency' => 'USD', 
			'returnUrl' => 'http://localhost/laravel/public/pagar_paypal',
            'cancelUrl' => 'http://localhost/laravel/public/pagar_paypal',
		))->send();

		dd($response);
	}
}
